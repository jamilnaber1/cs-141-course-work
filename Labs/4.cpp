//By: Jamil Naber & Wisam Abunimeh
#include <iostream>
using namespace std;

int main()
{
  int userInput;
  
  cout<< "Welcome to Pattern maker."<<endl;
  cout<<"Please enter an odd Integer for the Pattern Maker: "<<endl;
  cin >> userInput;
  
  for (int i = 0; i < userInput; i = i + 2)
  {
    
      for(int j = 0; j <= i; j++)
      {
        cout << "*";
      }
    cout<< endl;
  } 
  for (int i = userInput -2; i > 0; i = i - 2)
  {
    
      for(int j = 1; j <= i; j++)
      {
        cout << "*";
      }
    cout<< endl;
  } 
   
  
  return 0;
}