// Lab 7 
// Partner names: Jamil Naber & Wisam Abunimeh
//  
// Problems found:
// 1. Line 18, missing variables for function
// 2. Line 35, no Variables included
// 3. Line 21, i is being used to be added, not the number in the array
// 4. Line 20, 10 should not be their, max should
// 5. Line 20, i should be 0
// 6. Line 15, missing ";"
// 7. Line 19, not the right function name

#include <iostream>
using namespace std;

const int Max = 5;

// Catch the sum and the scores array as parameters below
void sumScoreValues(int scores[],int &sum)
{
   for( int i = 0; i < Max; i++) {
      sum = sum + scores[i];      
   }

   cout << "Sum is: "
        << sum << endl;
}

//-----------------------------------------------
int main() 
{
   int sum = 0;
   int scores[Max] = {72, 85, 93, 59, 78};

   // Pass the sum and the scores array to the function
   sumScoreValues(scores, sum);

   // Display the average of the score values
   cout << "Average is: " << 1.0 * sum / Max << endl;
}