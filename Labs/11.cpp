/*
 * CS 141 Lab 11 - Classes
 * 
 * 
 * 
 */
// By Jamil Naber and Wisam Abunimeh
#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

const int MaxSize = 81;

class Word{

    public:
    // implement constructors and member functions here
    // ...
    Word()
    {
      theWord[0] = '\0';
    }
    Word(char myWord[])
    {
      strcpy(this->theWord, myWord);
    }
    //getTheWord() {return theWord;}
    void print()
    {
      cout << theWord;
    }
    bool contains(char c)
    {
      if (strchr(theWord, c))
      {
        return true;
      }
      return false;
    }
    
    private:
        char theWord[ MaxSize];
};


int main() {

    // Step 1
    // For a score of 1 implement the constructors
    ifstream file;
    file.open("words.txt");
    char myWord[ MaxSize];
    Word wordsArray[ 1000];   // You'll need a default constructor for this to work

    int index = 0;
    while(file >> myWord){
        wordsArray[ index] = Word( myWord);  // Call the constructor
        index++;
    }

    // Validate that the file was read:
    cout << "The last word in the file is: " << myWord << endl << endl;
    
    // Step 2
    // For a score of 2, print all words contained in words.txt.
    // You will need to uncomment the code below and implement the print() member function 

    cout << "All words are: ";
    for(int i = 0; i < index; i++){
        wordsArray[i].print();            // call the print() member function that prints the C-string
        cout << " ";
    }
    cout << endl << endl;

    
    // Step 3
    // For extra credit (score of 3), display only words containing 'x'.
    // You will need to uncomment the code below and implement the contains() member function

    cout << "Words containing 'x' are: ";
    for(int i = 0; i < index; i++){
        if( wordsArray[ i].contains('x')) {   // call the contains() member function
            wordsArray[i].print();            // call the print() member function
            cout << " ";
        }
    }

    cout << endl;

    return 0;
}// end main()


