//  bubbleSort
//  By: Jamil Naber & Wisam Abunimeh
//  
//
#include <iostream>
#include <iomanip>		// for setw()
using namespace std;
 
const int ArraySize = 10;

void displayArray( int theArray[])
{
	// Display index values
    for( int i = 0; i < ArraySize; i++) {
        cout << setw( 3) << i;
    }
    cout << endl;
     
    // Display array contents
    for( int i = 0; i < ArraySize; i++) {
        cout << setw( 3) << theArray[i];
    }
    cout << endl
    	 << endl;
}//end displayArray()
 
 
// Swap two array elements.  Used in sorting.
void swap( int theArray [], int i, int j)
{
    int temp = theArray[i];     // store temporarily
    theArray[i] = theArray[j];
    theArray[j] = temp;
}
 
 
// Implement Bubble sort to put array values into ascending order
void bubbleSort( 
		int theArray[],    // Array of random numbers to be sorted
		int arraySize)     // How many numbers are in array
{
	// Make n-1 passes through the data
	for( int pass = 1; pass < arraySize-1; pass++) {
		
		// Examine adjacent pairs
		for( int i = 0; i < arraySize - pass; i++) {
			// If pair is out of order, swap them
			if( theArray[ i] > theArray[i+1]) {
				swap( theArray,i,i+1);
			}	
		}
			
	}//end for( int pass...			    
}//end bubbleSortbubbleSort
 
  
int main()
{
    int theArray[ ArraySize] = {26,1,7,11,4,10,23,8,19,13};     

    displayArray( theArray);     
    bubbleSort( theArray, ArraySize);
    displayArray( theArray);   
     
    return 0;
}
