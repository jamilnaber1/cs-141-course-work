#include <iostream>
using namespace std;
 
// The node structure to be used in the linked list
struct Node {
   int data;
   Node *pNext;
};

/*
 * DisplayEvenList function 
 * 1) prints out the elements in the list at even positions.
 * 2) The list has some elements
 * parameter(s): pHead: the start or head of the linked list
 * return: None
 */
void displayEvenList( Node *pHead)
{
   while( pHead != NULL) {
      cout << pHead->data << " ";
      pHead = pHead->pNext;
      pHead = pHead->pNext;
   }
   cout << endl; 
    
}

/* insertToLList function 
 * Inserts to the list AFTER the index specified. 
 * parameter(s): pHead - Points to the "head" or start of the Linked List.
 *               index - The index value we want to insert to AFTER the Linked List.
 *               value - The data element we are storing in the new node created.
 * return: None
 */
void insertToList( Node *pHead, int index, int value)
{
  Node *pTemp;
  Node *pHead2 = NULL;
  for(int i = 0; i < index; i++)
  {
    pHead = pHead->pNext;  
  }
  
  for(int i = 0; i < index +1; i++)
  {
    pHead2 = pHead->pNext; 
  }
   pTemp = new Node;
   pTemp->data = value;
   pTemp->pNext = pHead2;
   pHead->pNext = pTemp;
   //pHead -> pNext = pTemp;
   
  
}


//---------------------------------------
// You do not need to change anything below this line (in main)
// However, you will need to read it to understand how the Linked List 
// is pushing data elements to the list.
//---------------------------------------
void display(Node *pHead){
    while( pHead != NULL) {
      cout << pHead->data << " ";
      pHead = pHead->pNext;
   }
   cout << endl; 
}

int main()
{
   int userInput;
   Node *pHead = NULL;
   Node *pTemp;
   cout<<"Enter numbers for your list separated by space: (enter -1 to indicate end of list)" <<endl;
   cin >> userInput;
     
   // Keep looping until end of input flag of -1 is given
   while( userInput != -1) {
      // Store this number on the list
        pTemp = new Node;
        pTemp->data = userInput;
        pTemp->pNext = pHead;
        pHead = pTemp;
        cin >> userInput;
   }
   // choice for switch case
   int choice;
   cout<<"Enter choice: "<<endl;
   cout<<"1. Display Even elements" <<endl
       <<"2. Insert at nth position"<<endl
       <<"3. Exit"<<endl;
    cin>>choice;
    switch(choice){
      // Display list  
      case 1:
        {
        cout << "The Even elements in Linked List:" << endl;
        displayEvenList( pHead);
        break;
        }
      case 2:
        {
          // Code to prompt for index
          int index;
          cout<<"Enter the position for the new element: " <<endl;
          cin >> index;
   
          // Code to prompt for value
          int value;
          cout<<"Enter the value you want to insert: "<< endl;
          cin >> value;
   
          // Make the function call to insert the value after the specified index.
         insertToList( pHead, index, value);
   
         
         cout << endl << "The New Linked List:" << endl;
         display(pHead);
        }
    }      
   return 0;
}