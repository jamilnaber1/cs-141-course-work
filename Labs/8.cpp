// By Jamil Naber and Wisam Abunimeh
#include <iostream>
#include <cstring>
#include <string>
using namespace std;


void getNextWord(char * &pTheInput, char destination[])   
{
    char *pSpace = strchr( pTheInput, ' ');
    int length = pSpace - pTheInput;  
    strncpy( destination, pTheInput, length);
    destination[ length] = '\0';
    pTheInput = pSpace + 1;
}

int main()
{
  char first[ 81];
  char theString[81];
  int count;
  char *pTheString;
  int i = 0;
  cout<< "Welcome to Custom C string Functions."<<endl;
  cout<<"Enter a string of three or more words:";
  cin.getline(theString, 81);
  cout << "\n\n";
  
  theString[strlen(theString)] = '\0';
  pTheString = theString;
  for(int i = 0; i < strlen(theString);i++)
  {
      if(theString[i] == ' ')
      {
          count++;
      }
  }
  for(i = 0; i <= count; i++)
  {
    if(i != count)
    {
      getNextWord( pTheString, first);
      cout << first << endl;
    }
    else
    {
      cout << pTheString<< endl;
    }
  }
 return 0;
}
