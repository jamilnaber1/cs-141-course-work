#include <iostream>
#include <cstdlib>
#include <ctime> 
using namespace std;
// By: Jamil Naber &  Wisam Abunimeh 
// Global constant array of messages
string message[] = {
	"0. Get ready, you are about to learn something new.",
	"1. New friends are on their way.",
	"2. Yes, go ahead and make that phone call.",
	"3. Why isn't it right? Because you missed a step, that's why.",
	"4. Think carefully, as you might be missing something.",
	"5. Absolutely, go ahead.",
	"6. No way, do not proceed with that plan.",
	"7. Your program is ending now."};
	
int main()
{
  //int userInput;
  string Input;
  int randomPick;
  srand(time(0));
	//cout << "Welcome, let Magic 8-Ball show you the way! " 
  //<< "Think of a question, then enter a number in the range 0..7:"
  cout << "Think of a question, then hit enter key to get a message" << endl;
	cin.ignore();
	for(int i = 0; i < 6; i++)
  {
    randomPick = rand() % 8;
  
  cout << message[randomPick];
  cout <<endl;
  }
  
	return 0;
}