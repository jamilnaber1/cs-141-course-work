//
// Program 4: Solution for Coloroid, color-matching game
// 
// Instructions:
//    To run this program within Codio, first select the "Build and Run" option at the top 
//    of the window. You can ignore the following error messages that will appear:
//         Failed to use the XRandR extension while trying to get the desktop video modes
//         Failed to use the XRandR extension while trying to get the desktop video modes
//         Failed to initialize inotify, joystick connections and disconnections won't be notified
//    To see the graphical output then select the "Viewer" option at the top of the window.
//    
// For more information about SFML graphics, see: https://www.sfml-dev.org/tutorials
// Be sure to close the old window each time you rebuild and rerun, to ensure you
// are seeing the latest output. 
// 
// Author: Jamil Naber
// Class:  UIC CS-141, Fall 2018
// System: C++ on cloud-based Codio.com 
  
/****Lin:
 * Execution: 45 (-10 for not resetting the level)
 */ 
#include <SFML/Graphics.hpp> // Needed to access all the SFML graphics libraries
#include <iostream>          // Since we are using multiple libraries, now use std::
                             // in front of every cin, cout, endl, and string
#include <string>            // String functions
#include <sstream>           // Needed for the usage of ss stream inorder to convert int to string
#include <cstdio>            // For sprintf, "printing" to a string
#include <cstring>           // For c-string functions such as strlen()      

const int WindowXSize = 400;
const int WindowYSize = 500;
const int MaxBoardSize = 24;  // Max number of squares per side


class Square {
	public:
		// Default Constructor 
		Square()
		{
			size = 0;
			xPosition = 0;
			yPosition = 0;
			color = sf::Color::Black;
			isVisible = false;
			isCaptured = false;
			text = "";		
		}
	
		// Fully-qualified constructor, used to set all fields
		Square( int theSize, int theXPosition, int theYPosition, 
					  const sf::Color &theColor, bool theVisibility, std::string theText)
		{
			// Set the class fields
			size = theSize;
			xPosition = theXPosition;
			yPosition = theYPosition;
			color = theColor;
			isVisible = theVisibility;
			isCaptured = false;   // By default squares have not been captured
			text = theText;
			// Use the values to set the display characteristics of theSquare
			theSquare.setSize( sf::Vector2f(theSize, theSize));
			theSquare.setPosition( theXPosition, theYPosition);   // Set the position of the square
			theSquare.setFillColor( theColor);
		}
			
		// Get (accessor) functions
		sf::RectangleShape getTheSquare() { return theSquare; }
		int getSize() { return size; }
		int getXPosition() { return xPosition; }
		int getYPosition() { return yPosition; }
		sf::Color& getColor() { return color; }
		bool getIsVisible() { return isVisible; }
		bool getIsCaptured() { return isCaptured; }
		std::string getText() { return text; }
	
		// Set (mutator) functions
		void setSize( int theSize) { 
			size = theSize; 
			theSquare.setSize( sf::Vector2f(theSize, theSize));
		}
		void setXPosition( int theXPosition) { 
				xPosition = theXPosition; 
				theSquare.setPosition( theXPosition, yPosition);   // Set the position of the square
		}
		void setYPosition( int theYPosition) { 
				yPosition = theYPosition; 
				theSquare.setPosition( xPosition, theYPosition);   // Set the position of the square
		}
		void setColor( sf::Color & theColor) { 
				color = theColor; 
				theSquare.setFillColor( theColor);    // Also update the color on the square itself
		}
		void setVisibility( bool theVisibility) { isVisible = theVisibility; }
		void setIsCaptured( bool isCaptured) { this->isCaptured = isCaptured; }
		void setText( std::string theText) { text = theText; }

		// Utility functions
		void displayText( sf::RenderWindow *pWindow, sf::Font theFont, sf::Color theColor, int textSize);
	
	private:
		int size;
		int xPosition;
		int yPosition;
		sf::Color color;
		bool isVisible;
		bool isCaptured;   // Indicates whether or not it is part of the captured area
		std::string text;
		sf::RectangleShape theSquare;

}; //end class Square


void Square::displayText( 
			sf::RenderWindow *pWindow,   // The window into which we draw everything
			sf::Font theFont,            // Font to be used in displaying text   
			sf::Color theColor,          // Color of the font
			int textSize)                // Size of the text to be displayed
{	
		// Create a sf::Text object to draw the text, using a sf::Text constructor
		sf::Text theText( text,        // text is a class data member
								      theFont,     // font from a font file, passed as a parameter
								      textSize);   // this is the size of text to be displayed

		// Text color is the designated one, unless the background is Yellow, in which case the text
		// color gets changed to blue so we can see it, since we can't see white-on-yellow very well
		if( this->getColor() == sf::Color::Yellow) {
				theColor = sf::Color::Blue;
		}
		theText.setColor( theColor);

		int theXPosition = xPosition + (size / 2) - (strlen(text.c_str()) * theText.getCharacterSize()) / 2;

		int theYPosition = yPosition + (size - theText.getCharacterSize()) / 2;

		int offset = 5;
		theText.setPosition( theXPosition + offset, theYPosition - offset);

		pWindow->draw( theText);
}


// Initialize the font
void initializeFont( sf::Font &theFont)
{
    // Create the global font object from the font file
		if (!theFont.loadFromFile("arial.ttf"))
		{
			 std::cout << "Unable to load font. " << std::endl;
			 exit( -1);
		}	
}


// Get a random number, and use that to return the corresponding color
// The SFML Color documentation page indicates that the six valid colors 
// are: Black, Red, Green, Blue, Yellow, Cyan
sf::Color getRandomColor()
{
		sf::Color theColor;
    // Get a random number in the range 0..5
		switch (random()%6 ) {
				case 0: theColor = sf::Color::Black;  break;
				case 1: theColor = sf::Color::Red;    break;
				case 2: theColor = sf::Color::Green;  break;
				case 3: theColor = sf::Color::Blue;   break;
				case 4: theColor = sf::Color::Yellow; break;
				case 5: theColor = sf::Color::Cyan;   break;
				default: std::cout << "Invalid random number, exiting program..." << std::endl;
						exit( -1);
						break;
		}
	return theColor;
}//end getRandomColor()


// Convert sf::Color to a string, for ease of printing when debugging.  When calling this
// you must previously have created an array of char that you send as the second parameter
// to store the resulting color name to use (for instance) in printing debugging information.
void getColorAsString( 
				sf::Color theColor,      // The sf::Color
				char theColorName[ 81])  // Resulting string will be stored here
{
		if( theColor == sf::Color::Black)      strcpy( theColorName, "Black");
		else if( theColor == sf::Color::Red)   strcpy( theColorName, "Red");
		else if( theColor == sf::Color::Green) strcpy( theColorName, "Green");
		else if( theColor == sf::Color::Blue)  strcpy( theColorName, "Blue");
		else if( theColor == sf::Color::Yellow)strcpy( theColorName, "Yellow");
		else if( theColor == sf::Color::Cyan)  strcpy( theColorName, "Cyan");
		else strcpy( theColorName, "Invalid color");
}


// void funstion that sets uo the squares using the Consturtor in the Class. The level and squaresArray
// ... are passed.
void setSquaresUp(int level,Square squaresArray[])
{
  int index = 0; // index of the array
  sf::Color color;
  // the first loop, i, is for the bounds of the y axis, and the seacond, j, is for the shift in the x-axis
  for(int i = 0; i < level; i++) 
  {
    for(int j = 0; j < level; j++)
    {
      std::stringstream ss; //string stream set
      ss <<index; // inserts index to ss
      std::string str = ss.str(); // adds the ss to string  str
      color = getRandomColor(); //gets a random color
      //set the values of the constutors of each element in squaresArray
      squaresArray[index++]= Square(WindowXSize/level, WindowXSize/level * j, (WindowYSize -100)/level * i, color, false, str);
     }// end of first loop
  }//end of seacond loop
}//end of function setSquaresUp


// void function findingIfVisible is made to see what squares are adjacent to all the captured squares.
// It will use squaresArray, level, currentSqaure, and selectedColor.
// This function will also be recursive, meaning it will call itself. 
void findingIfVisible(Square squaresArray[],int level, int currentSqaure , sf::Color selectedColor)
{
  int adjacent[4] = {-1, -level, 1, level}; //set an array for the 4 areas that are considered adjacent
  //checks the condition if the current sqaure has been set as captured
  if(squaresArray[currentSqaure].getIsCaptured() == true)
  {
    //for loop to check every possible condition of an sqaure.
    //if a sqaure falls into one of the if statments it will skip it with a 
    //contiune statement, and will noew go to the next possible adjacent sqaure
    for(int i = 0; i < 4; i++)
    {
      //if statements to check for speical circumstances
      //checks for right boarder
      if( currentSqaure % level == level - 1 && i == 2)
      {
        continue;
      }
      //checks for left boarder
      if( currentSqaure % level == 0 && i==0)
      {
        continue;
      }
      //adds the adjacent shift to the sqaure
      int adjSquare = currentSqaure + adjacent[i];
      // checks if the adjacent is with-in bounds of the window
      if( (adjSquare < 0) || (adjSquare >= (level * level)) )
      {
        continue;
      }
      //checks if the square has bee captured yet and if the color is the same as the selected color
      if(squaresArray[adjSquare].getIsCaptured() != true && squaresArray[adjSquare].getColor() == selectedColor)
      {
        //set it as captured
        squaresArray[adjSquare].setIsCaptured(true);
        //recursive statement
        findingIfVisible(squaresArray, level, adjSquare, selectedColor);
      }
    }//end of for loop 
  }
}//end of function findingIfVisible


// void function settingCaptureArea sets the captured area to the selectedColor
// squaresArray, currentSqaure, selectedColor are in the scope 
void settingCaptureArea(Square squaresArray[], int currentSqaure, sf::Color selectedColor)
{
  if(squaresArray[currentSqaure].getIsCaptured() == true)
  {
    squaresArray[currentSqaure].setColor(selectedColor);
  }
}//end of settingCaptureArea function


// void function locationOfClick finds the location of where the user clicks, and finds in terms of index
// In the scope there is level, squaresArray, Vector2i, reference location and reference clickAmount
void locationOfClick(int level, Square squaresArray[], sf::Vector2i localPosition, int &location, int &clickAmount)
{
  //for loop runs though the array size, and locates the click location
  for(int i = 0; i < level * level; i++ )
  {
    //finds the position of the click y and checks if its greater than a sqaure in squaresArray
    if(localPosition.y < squaresArray[i].getYPosition() + WindowXSize/level)
    {
      //finds the position of the click x and checks if its greater than a sqaure in squaresArray
      if(localPosition.x < squaresArray[i].getXPosition() + WindowXSize/level)
         {
           location = i; //set where the location is
           break; // breaks out as soon it finds the location
          }
     }
  }//end of loop
  clickAmount++; //increments the clickAmount
}//end of function locationOfClick


// boolean retrun function checkIfCleared check what the coniditon of the board is at all times.
// It will see if the game is completed, and whether the level needs to be incermented
// In the scope squaresArray, arraySize, settingUpBoard, reference level, selectedColor, par and clickAmount
bool checkIfCleared(Square squaresArray[], int arraySize, bool settingUpBoard, int &level, sf::Color selectedColor, int par, int clickAmount)
{
  // For loop that will check each sqaure in the arraySize
  for( int i=0; i < arraySize; i++)
  {
    //checks whether the color is not equal to the selectedColor
    if(squaresArray[i].getColor() != selectedColor) 
      {
        return false; //returns false
        break; // backup
      }
  }
  if(level >= 24)
  {
    return true;
  }
  //checks if the Move (clickAmount) it less than par
  if(clickAmount <= par)
  {
    level++; //incerments level
  }
  return true; // returns true
}//end of checkIfCleared function 


int main()
{	
		char aString[ 81];  // c-string to hold concatenated output of character literals
    int location = 0; // Initialize and set location of the mouse click
    int clickAmount = 0; // Initialize and set clickAmount for the amount the user clicks
    sf::Color selectedColor; // created selectedColor for class Color to see what color has been selected
    int par; // initialize par
    
    // Create and initialize the font, to be used in displaying text.
		sf::Font font;  
		initializeFont( font);

    // Create the destination window
		sf::RenderWindow window(sf::VideoMode(WindowXSize, WindowYSize), "Program 4: Color Match");

    // Create the messages label at the bottom of the screen, to be used in displaying debugging information.
		sf::Text messagesLabel( "", font, 20);
    // Make a text object from the font
		messagesLabel.setColor( sf::Color::White);
    // Place text at the bottom of the window. Position offsets are x,y from 0,0 in upper-left of window
		messagesLabel.setPosition( 0, WindowYSize - messagesLabel.getCharacterSize() - 5);  
		int level = 4; // level is number of squares per side.  There are level * level number of squares.    
		int arraySize;
		// Create array of Square objects to be the max size it will ever be.
		Square squaresArray[ MaxBoardSize * MaxBoardSize];
		bool mouseIsAlreadyPressed = false;
		bool settingUpBoard = true;
  
    // Run the program as long as the window is open.  This is known as the "Event loop".
		while (window.isOpen())
		{
      // check all the window's events that were triggered since the last iteration of the loop
      arraySize = level * level; //set arraySize inside the event loop so when level is incermented the arraySize will update
      
				sf::Event event;
				while (window.pollEvent(event)) 
        {
          // There was a "close requested" event, so we close the window
						if (event.type == sf::Event::Closed) 
            { 
							window.close(); 
						}  	
				}
        //if statement checks if the board needs to be setup
				if( settingUpBoard == true) 
        {
          clickAmount = 0;// resets the click amount
          setSquaresUp(level,squaresArray); //resets the squares
				}
			  // Clear the window, erasing what is displayed
				window.clear();
        
        // Redraw all screen components to the background
				for( int i=0; i< arraySize; i++) 
        {
          window.draw( squaresArray[ i].getTheSquare());
				}
        
        // Display the coordinates for the mouse when the mouse left button is pressed
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !mouseIsAlreadyPressed)
				{
            // Set a flag to indicate mouse button has been pressed.  This is used to only increment the
						// numberOfMoves once, and not every time through the event loop.  This flag gets turned off
						// once the mouse is no longer pressed.
						mouseIsAlreadyPressed = true;
						
            // Get the current mouse x,y position and store into a std::string to be displayed as text in debugging.
						sf::Vector2i localPosition = sf::Mouse::getPosition(window);   // Vector2i stores x,y value
            
            //finds the locatin of the click, and will be reference back to main as the varible location
            locationOfClick(level, squaresArray, localPosition, location, clickAmount);
            
            //sets the selectedColor to the location in squaresArray, and will get the color of that location 
            selectedColor = squaresArray[location].getColor();
            //set the start corner to be as the captured area
						squaresArray[0].setIsCaptured(true);
          
            // Seeing if any of the sqaures around beganning of the capture area is the same color, and set them to be the capture area
            findingIfVisible(squaresArray, level, 0, squaresArray[0].getColor());
            settingCaptureArea(squaresArray, 0, squaresArray[0].getColor());
          
            // the for loop will run though the array size. It will call the functions findingIfVisible and settingCaptureArea
            for(int i = 0; i < level * level; i++)
            {
              //checks for visible sqaure, setting capture area and setting color for captured sqaure 
              findingIfVisible(squaresArray, level, i, selectedColor);
              settingCaptureArea(squaresArray, i, selectedColor);
            }
				}
        par = level * 2;  //set par
        sprintf( aString, "(Par %d)  Move %d", par, clickAmount); //Display Par and Move information
				// Set mouse press status to false.  This is used to only go through the mousedown code a single
				// time when the mouse is pressed, rather than running that code every time through the event loop.
				if (! sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouseIsAlreadyPressed) {
						mouseIsAlreadyPressed = false;
				}
			
				// Display previously constructed string at the bottom of the screen 
				messagesLabel.setString( aString);                    // Store the string into the messagesLabel
				window.draw( messagesLabel);                          // Display the messagesLabel
			  
				// Display the background frame buffer, replacing the previous RenderWindow frame contents.
				// This is known as "double-buffering", where you first draw into a background frame, and then
				// replace the currently displayed frame with this background frame.
				window.display();
        settingUpBoard = checkIfCleared(squaresArray, arraySize, settingUpBoard, level, selectedColor , par, clickAmount);
		}//end while( window.isOpen())

		return 0;
}//end main()