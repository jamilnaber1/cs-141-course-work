/*  mastermind.cpp
 
    Class: CS 141, Fall 2018  
	  Wed 9am lab
    System: Codio.com
    Author: Jamil Naber
 
    45 points Grading Rubric for Programming Style
 
        10 Meaningful identifier names
        10 Comments: Has header.  Comments on each block of code
        10 Functional Decomposition
        10 Appropriate data and control structures
         5 Code Layout: Appropriate indentation and blank lines
*/
#include <iostream>
#include <cstdlib>
#include <sstream>
using namespace std;
/***Mallavarapu: 
 * Header information missing your information in Stage 1
 * 
 * */

//-------------------------------------------------------------------
//declaring fuctions
void intro();
string userChoiceFunc(string);
string genOrFindNum(string);
bool mainGameComponet(string);
int inPlaceFunc(string, string);
int outofPlaceFunc(string, string, int);
void finalResult(bool);
  
int main()
{
  //initializing variables
  string userChoice;
  string threeDigitNum;
  bool guessRight;
  
  intro();//function that has the game intro and rules
  userChoice = userChoiceFunc(userChoice); //Grab's the user Choice
  threeDigitNum = genOrFindNum(userChoice); //uses userChoice to find out what threeDigitNum
  cout << "\n\nInput of 000 displays the hidden digits.  Input of 999 exits the program." << endl;
  guessRight = mainGameComponet(threeDigitNum);// The guessing component of the game
  finalResult(guessRight);// the result the user recived after playing
	return 0;
}//end main()


// The function for introduction to the game, cout statements
void intro()
{
  cout << "Program: 2 MasterMind" << endl;
	cout << "The program selects 3 distinct random digits 0..9." << endl;
	cout << "On each turn you guess 3 digits.  The program indicates" << endl;
  cout << "how many are correct. You have 10 moves to guess the number." << endl;
  cout << "Good luck!" << endl;
}


// function that asked and recives the user input
string userChoiceFunc(string userChoice)
{
  cout << "\nPress 's' to set the three digits, or 'r' to randomize them: ";
  cin >> userChoice;
  return userChoice; // returns the user choice
}


// the function that will take the user input to determine how to set the number to be guessed
string genOrFindNum(string userChoice) 
{
  string threeDigitNum; // variable that holds the games number
  int userEnteredNum;
  int singleRandValue; // the single digit randomize
    if(userChoice == "s")
  {
    cout << "Enter three distinct digits each in the range 0..9 (e.g. 354): ";
    cin >> userEnteredNum;
    stringstream ss; // sstream is used to convert the int to a string without errors
    ss << userEnteredNum; // ss will be have userEnteredNum set to it
    threeDigitNum = ss.str(); // the substr of ss is equal to threeDigitNum
  }
  else if (userChoice == "r")
  {
    for (int i=0; i < 3; i++) // Runs three times to get a random three digit number
    {
        singleRandValue = rand() % 10; // rand value
        stringstream ss; // ss stream used to have convertion from int to string
        ss << singleRandValue;   
        threeDigitNum.insert(i, ss.str()); // insert is used to put the recent digit at the end of the string      
    }
  }
  return threeDigitNum; // returns the games set number 
}


//The function that has the outputs and function of the guessing game
//set as a bool to return a ture or false inplace as 0's
bool mainGameComponet(string threeDigitNum)
{
  string userGuess; // string that holds the user guess during the game
  int i = 1;
  cout << "\n\n\t\t\t    In place   Out of place" << endl;
  cout << "\t\t\t    --------   ------------" << endl;
  
  // a do while is used for this run once before the condition is checked
  do 
  {
    cout << i << ". Your guess:";
    cin >> userGuess;
    if (userGuess == "000") // checks if then user entered "000"
    {
      cout<< "\n   Hidden digits are: " << threeDigitNum << endl;
    }
    else if(userGuess == "999") // checks if then user entered "999"
    {
      cout << "\n   Exiting loop..." << endl;
      return 0; //returns a false statment for the boolean function
    }
    else
    {
      int inPlace = inPlaceFunc(userGuess,threeDigitNum); // checks what digits are in-place
      int outOfPlace = outofPlaceFunc(userGuess, threeDigitNum, inPlace);
      cout << "\t   You entered: "<< userGuess <<"\t"<< inPlace <<"\t   "<< outOfPlace << endl;
      i++;
    }
  }while(userGuess != threeDigitNum && i != 11); 
  // the conditon depends whether userGuess is equal to threeDigitNum and if  i is equal to 11 
    if (userGuess == threeDigitNum) //this checks if the userGuess is correct
    {
      return 1; //returns a truth value for the function
    }
  return 0; //returns a false value if nothing above returns first
}


//Funtion that compares the user guess and the set game number if they are in place
int inPlaceFunc(string userGuess, string threeDigitNum)
{
  int amountOfTruth = 0; //need to set this variable to 0 in-order not to get an error
  string userGuessSub;
  string threeDigitNumSub;
  for(int j = 0; j < 3; j++) // loop runs 3 times to check the corresponding j in both arrays index
  {
    // if statment checks if the number is the same at the same index of both arrays
    if (userGuess.substr(j, 1) == threeDigitNum.substr(j, 1) ) 
    {
      amountOfTruth++; // an accumulator is used if true
    }
  }
  return amountOfTruth; // returns the accumulated value of amountOfTruth
}


// Function that compares the user guess to the set number to check what digits are out of place
int outofPlaceFunc(string userGuess,string threeDigitNum, int inPlace)
{
  int outOfPlace = 0; //need to set this variable to 0 in-order not to get an error
  for (int k = 0; k < 3; k++) // for loop is to run 3 times with the value k used as the counter
  {
    // we have an nested for loop to check all the digits in threeDigitNum for one value of userGuess
    for (int l = 0; l < 3; l++)
    {
      if(userGuess.substr(k, 1) == threeDigitNum.substr(l,1))
      {
          outOfPlace++; // accumulator if true
      }
    }
  }
  //the function will still count inPlace numbers as outOfPlace, but snice we know what the value...
  //... of inPlace we can just subtract that out
  outOfPlace = outOfPlace - inPlace;// the real value of outOfPlace is calulated
  return outOfPlace; //returns outOfPlace
}


// Funtion that shows the result of the overall game, win or lose
void finalResult(bool guessRight)
{ 
  if (guessRight == true) // if the boolean of geussRight is true
  {
    cout << "\n*** Congratulations! ***" << endl;
  }
  else if (guessRight == false)// if the boolean of geussRight is false
  {
    cout <<"\n   Better luck next time.";
  }
  cout << "\n\nExiting program...";
}