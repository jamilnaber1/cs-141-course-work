/* prog5Wumpus.cpp
 
 Description: Navigate a maze to kill the Wumpus.
 Implementation of a 1972 game by Gregory Yob.
 
 Results of a program run are shown below:

   Author: Jamil Naber
   Class:  UIC CS-141, Fall 2018
   System: C++ on cloud-based Codio.com     
 */
#include <iostream>
#include <iomanip>   // for setw()
#include <cctype>    // for toupper()
#include <cstdlib>   // for rand() and srand()
#include <ctime>     // to seed random number generator with time(0)

using namespace std;

// global constants
#define MAX_LINE_LENGTH 81
#define NUMBER_OF_ROOMS 20

const int adjacentRooms[21][3]={ {0,0,0}, {2,5,8},
                                 {1,3,10}, {2,4,12},
                                 {3,5,14}, {1,4,6},
                                 {5,7,15}, {6,8,17},
                                 {1,7,9}, {8,10,18},
                                 {2,9,11}, {10,12,19},
                                 {3,11,13}, {12,14,20},
                                 {4,13,15}, {6,14,16},
                                 {15,17,20}, {7,16,18},
                                 {9,17,19}, {11,18,20},
                                 {13,16,19} 
                               };

//create class Cave
class Cave
{
  public:
    // Default Constructor 
    Cave()
    {
      person = 0;
      bottomlessPit1 = 0;
      bottomlessPit2 = 0;
      superBats1 = 0;
      superBats2 = 0;
      wumpus = 0;
      arrow = -1;
      endgame = false;
    }
  
    // Get (accessor) functions
    int getPerson(){ return person; }
    int getBottomlessPit1() { return bottomlessPit1; }
    int getBottomlessPit2() { return bottomlessPit2; }
    int getSuperBats1() { return superBats1; }
    int getSuperBats2() { return superBats2; }
    int getWumpus() { return wumpus; }
    int getArrow() { return arrow; }
    int getEndGame(){ return endgame; }
  
    // Set (mutator) functions
    void setPerson ( int isPerson)
    {
      this -> person = isPerson;
    }
    void setBottomlessPit1( int isBottomlessPit1)
    {
      this -> bottomlessPit1 = isBottomlessPit1;
    }
    void setBottomlessPit2( int isBottomlessPit2)
    {
      this -> bottomlessPit2 = isBottomlessPit2;
    }
    void setSuperBats1( int isSuperBats1)
    {
      this -> superBats1 = isSuperBats1;
    }
    void setSuperBats2( int isSuperBats2)
    {
      this -> superBats2 = isSuperBats2;
    }
    void setWumpus( int isWumpus)
    {
      this -> wumpus = isWumpus;
    }
    void setArrow ( int isArrow)
    {
      this -> arrow = isArrow;
    }
    void setEndGame( int isEndGame)
    {
      this -> endgame = isEndGame;
    }
  
  private:
    int person;
    int bottomlessPit1;
    int bottomlessPit2;
    int superBats1;
    int superBats2;
    int wumpus;
    int arrow;
    bool endgame;
};




//--------------------------------------------------------------------------------
void displayCave()
{
        cout<< "       ______18______             \n"
            << "      /      |       \\           \n"
            << "     /      _9__      \\          \n"
            << "    /      /    \\      \\        \n"
            << "   /      /      \\      \\       \n"
            << "  17     8        10     19       \n"
            << "  | \\   / \\      /  \\   / |    \n"
            << "  |  \\ /   \\    /    \\ /  |    \n"
            << "  |   7     1---2     11  |       \n"
            << "  |   |    /     \\    |   |      \n"
            << "  |   6----5     3---12   |       \n"
            << "  |   |     \\   /     |   |      \n"
            << "  |   \\       4      /    |      \n"
            << "  |    \\      |     /     |      \n"
            << "  \\     15---14---13     /       \n"
            << "   \\   /            \\   /       \n"
            << "    \\ /              \\ /        \n"
            << "    16---------------20           \n"
            << endl;
}//end displayCave()


//--------------------------------------------------------------------------------
void displayInstructions()
{
    cout<< "Hunt the Wumpus:                                             \n"
        << "The Wumpus lives in a completely dark cave of 20 rooms. Each \n"
        << "room has 3 tunnels leading to other rooms.                   \n"
        << "                                                             \n"
        << "Hazards:                                                     \n"
        << "1. Two rooms have bottomless pits in them.  If you go there you fall and die.  \n"
        << "2. Two other rooms have super-bats.  If you go there, the bats grab you and    \n"
        << "   fly you to some random room, which could be troublesome.  Then those bats go\n"
        << "   to a new room different from where they came from and from the other bats.  \n"
        << "3. The Wumpus is not bothered by the pits or bats, as he has sucker feet and   \n"
        << "   is too heavy for bats to lift.  Usually he is asleep.  Two things wake      \n"
        << "   him up: Anytime you shoot an arrow, or you entering his room.  When he      \n"
        << "   wakes he moves if he is in an odd-numbered room, but stays still otherwise. \n"
        << "   After that, if he is in your room, he eats you and you die!                 \n"
        << "                                                                               \n"
        << "Moves:                                                                         \n"
        << "On each move you can do the following, where input can be upper or lower-case: \n"
        << "1. Move into an adjacent room.  To move enter 'M' followed by a space and      \n"
        << "   then a room number.                                                         \n"
        << "2. Shoot your guided arrow through a list of up to three adjacent rooms, which \n"
        << "   you specify.  Your arrow ends up in the final room.                         \n"
        << "   To shoot enter 'S' followed by the number of rooms (1..3), and then the     \n"
        << "   list of the desired number (up to 3) of adjacent room numbers, separated    \n"
        << "   by spaces. If an arrow can't go a direction because there is no connecting  \n"
        << "   tunnel, it ricochets and moves to the lowest-numbered adjacent room and     \n"
        << "   continues doing this until it has traveled the designated number of rooms.  \n"
        << "   If the arrow hits the Wumpus, you win! If the arrow hits you, you lose. You \n"
        << "   automatically pick up the arrow if you go through a room with the arrow in  \n"
        << "   it.                                                                         \n"
        << "3. Enter 'R' to reset the person and hazard locations, useful for testing.     \n"
        << "4. Enter 'C' to cheat and display current board positions.                     \n"
        << "5. Enter 'D' to display this set of instructions.                              \n"
        << "6. Enter 'P' to print the maze room layout.                                    \n"
        << "7. Enter 'X' to exit the game.                                                 \n"
        << "                                                                               \n"
        << "Good luck!                                                                     \n"
        << " \n"
        << endl;
}//end displayInstructions()


//--------------------------------------------------------------------------------
// int return function randomNumber generator's a random number everytime it is called
// it will return the int of varibale randomNum 
int randomNumber()
{
  int randomNum = rand() % 20 + 1;    // Gets a random Value from 1 to 20
  return randomNum;                   // Return
}//end of randomNum()


//--------------------------------------------------------------------------------
//int return function lowestAdjacent() has a parameter of int valueToCompare in it's scope
//this function takes in a room number and finds its lowest adjacent room 
int lowestAdjacent(int valueToCompare)
{
  int lowestAdjacent = 20;      //set to highest room
  
  //for loop that will check each adjacent room, and determine the lowest number
  for(int i = 0; i < 3; i++)    
  {
    //Compare statement
    if (adjacentRooms[valueToCompare][i] < lowestAdjacent)  
    {
      lowestAdjacent = adjacentRooms[valueToCompare][i];
    }
  }
  return lowestAdjacent;  //returns the lowest numbered adjacent room
} //end of lowestAdjacent()


//--------------------------------------------------------------------------------
//void function displayMove has parameter moveNumber in its scope
//this function displays the room number
void displayMove(int moveNumber)
{
  cout << moveNumber << ". Enter your move (or 'D' for directions): ";
}//end of displayMove()


//--------------------------------------------------------------------------------
//void function setSuperBatsLocation has a cave parameter in its scope
//this function set random location for both super bats 1 and 2
void setSuperBatsLocation(Cave &cave)
{
  int location;
  location = randomNumber();        //get random number
  cave.setSuperBats1(location);     //set random number to superBats1
  do
  { 
    location = randomNumber();                 //get random number
  }while(cave.getSuperBats1() == location);    //check that random location does not match superBats1
  cave.setSuperBats2(location);                //set random location to superBats2
}//end of setSuperBatsLocation()


//--------------------------------------------------------------------------------
//void function setBottomlessPit2 has a cave parameter in its scope
//this function set random location for bottomless pits 1 and 2 
void setBottomlessPitLocation(Cave &cave)
{
  int location;
  location = randomNumber();         //get random number
  cave.setBottomlessPit1(location);  //set random number to BottomlessPit1
  do
  { 
    location = randomNumber();                     //get random number  
  }while(cave.getBottomlessPit1() == location);    //check that random location does not match bottomlessPit1
  cave.setBottomlessPit2(location);                //set random location to bottomlessPit2
}//end of setBottomlessPitLocation()


//--------------------------------------------------------------------------------
//void function setWumpusLocation() has a cave parameter in its scope
//this function set random location for wumpus
void setWumpusLocation(Cave &cave)
{
  int location;
  location = randomNumber();    //get random number
  cave.setWumpus(location);     //set random number to wumpus
}//end of setWumpusLocation


//--------------------------------------------------------------------------------
//void function setWumpusLocation() has a cave parameter in its scope
//this function set the player location
void setPlayerLocation(Cave &cave)
{
  int location;
  do
  {
    location = randomNumber();//get random number
  }while((location == cave.getWumpus()) || (location == cave.getBottomlessPit1()) || (location == cave.getBottomlessPit2()) || (location == cave.getSuperBats1()) || (location == cave.getSuperBats2()));
  //checks if the player is spawning in a room with hazards

  cave.setPerson(location);   //set random number to player
}


//--------------------------------------------------------------------------------
//void function displayCurrentLocations() has a cave parameter in its scope
//this function displays the current placements of objects of the game
void displayCurrentLocations(Cave &cave)
{
  cout << "\nCheating! Game elements are in the following rooms: " << endl;
  int outputs[7]= {cave.getPerson(), cave.getWumpus(), cave.getSuperBats1(), cave.getSuperBats2(), cave.getBottomlessPit1(), cave.getBottomlessPit2(), cave.getArrow()};
  cout << "\n"<< "Player "<< "Wumpus " << "Bats1 "<< "Bats2 " << "Pit1 " << "Pit2 " << "Arrow " << endl;
  cout << setw(4)<< cave.getPerson() << setw(7)<< outputs[1] << setw(6)<< outputs[2] << setw(6) << outputs[3] << setw(6) << outputs[4] << setw(5) << outputs[5] << setw(5) << outputs [6];
  cout << endl;
}//end of displayCurrentLocations()


//--------------------------------------------------------------------------------
//void function resetGame() has a cave parameter in its scope
//this function takes in user data, and reset the locations  
void resetGame(Cave &cave)
{
  int inputarray[7];   //array to hold the inputs
  cout << "\n(Remember arrow value of -1 means it is with the person.)" << endl;
  cout << "Enter the 7 room locations (1..20) for player, wumpus, bats1, bats2, pit1, pit2, and arrow: " << endl;
  //for loop to grab all the inputs
  for(int i = 0; i < 7; i++)
  {
    cin >> inputarray[i];
  }//end of for loop
  //set all new inputs to the objects
  cave.setPerson(inputarray[0]);
  cave.setWumpus(inputarray[1]);
  cave.setSuperBats1(inputarray[2]);
  cave.setSuperBats2(inputarray[3]);
  cave.setBottomlessPit1(inputarray[4]);
  cave.setBottomlessPit2(inputarray[5]);
  cave.setArrow(inputarray[6]);
}//end of resetGame()


//--------------------------------------------------------------------------------
//void function playersCurrentRoom has a cave parameter in its scope
//this function displays the current room the player is in
void playersCurrentRoom(Cave cave)
{
  cout << "You are in room " << cave.getPerson() << ". ";
}//end of playersCurrentRoom


//--------------------------------------------------------------------------------
//void function adjacentHazards has a cave parameter in its scope
//the function check the if there is hazards in adject rooms 
void adjacentHazards(Cave cave)
{
  //all three four loops check the adjacent rooms for each of its hazards
  //checks for wumpus
  for(int i = 0; i < 3; i++)
  {
    if (adjacentRooms[cave.getPerson()][i] == cave.getWumpus())
    {
      cout <<"You smell a stench. ";
    }
  }//end of for loop
  //checks for bottomless pit's
  for(int i = 0; i < 3; i++)
  {
    if (adjacentRooms[cave.getPerson()][i] == cave.getBottomlessPit1() || adjacentRooms[cave.getPerson()][i] == cave.getBottomlessPit2())
    {
      cout <<"You feel a draft. ";
    }
  }//end of for loop
  //checks for super bats
  for(int i = 0; i < 3; i++)
  {
    if (adjacentRooms[cave.getPerson()][i] == cave.getSuperBats1() || adjacentRooms[cave.getPerson()][i] == cave.getSuperBats1())
    {
      cout <<"You hear rustling of bat wings. ";
    }
  }//end of for loop
}//end of adjacentHazards


//--------------------------------------------------------------------------------
//void function movingPlayer() has a cave parameter and int newRoom in its scope
//set the players new location after mving
void movingPlayer(Cave &cave, int newRoom)
{
  cave.setPerson(newRoom);
}//end of movingPlayer()


//--------------------------------------------------------------------------------
//boolean function checkIfValidRoom() has a cave parameter and int newRoom in its scope
//checks if the move that the user input is valid room choice
bool checkIfValidRoom(Cave cave, int newRoom)
{
  //for loop that checks if the adjacent room to the original room, and checks if its a valid move
  for(int i = 0; i < 3; i++)
  {
    if (newRoom == adjacentRooms[cave.getPerson()][i])
    {
      return true;
      break;
    }
  }//end of for loop
  cout << "Invalid move.  Please retry. " << endl;
  return false;
}//end of checkIfValidRoom()


//--------------------------------------------------------------------------------
//void function shootingInstru() has no parameter's
//It displays the instructions for shooting
void shootingInstru()
{
  cout << "\nEnter the number of rooms (1..3) into which you want to shoot, followed by the rooms themselves:\n" ;
}//end of shootingInstru()


//--------------------------------------------------------------------------------
//void function checkPerson has parameter's of cave, int reference amountOfRooms, and default int mode
//function checks if the person got shoot by the arrow
void checkPerson(Cave &cave, int &amountOfRooms, int mode = 0)
{
  //if the person location is equal to arrow location means the player shoot themselves
  if(cave.getPerson() == cave.getArrow())
  {
    //if statement to determine which output comes out
    if(mode == 1)
    {
      cout << "\nYou just shot yourself, and are dying. " << endl;
      cout << "It didn't take long, you're dead. " << endl;
    }
    else
    {
      cout << "You just shot yourself. " <<endl;
      cout << "Maybe Darwin was right.  You're dead. " <<endl;
    }
    amountOfRooms = 0;       //set the shoot function 
    cave.setEndGame(true);   //set this to be the end of the game
  }
}//end of checkPerson()


//--------------------------------------------------------------------------------
//void function setWumpusAfter() has a cave parameter
//this is to set the wumpus new location after the arrow has been shot
void setWumpusAfter(Cave &cave)
{
  int lowestAdj;
  lowestAdj = lowestAdjacent(cave.getWumpus()); 
  cave.setWumpus(lowestAdj);                    //set the lowest adjacent room as the new location
}//end of setWumpusAfter


//--------------------------------------------------------------------------------
//void function checkWampus() has a cave parameter
//this is to check if the player has enter the wumpus room
void checkWampus(Cave &cave)
{
  //the case for a even number
  if(cave.getWumpus() == cave.getPerson() && cave.getPerson() % 2 == 0)
  {
    cout << "You briefly feel a slimy tentacled arm as your neck is snapped. It is over. " << endl;
    cave.setEndGame(true); //sets the end game to true
  }
  //the case for an odd number
  if(cave.getWumpus() == cave.getPerson() && cave.getPerson() % 2 != 0)
  {
    cout << "\nYou hear a slithering sound, as the Wumpus slips away. " << endl;
    cout << "Whew, that was close!" << endl;
    setWumpusAfter(cave); //resets location
  }
  if(cave.getWumpus() == cave.getArrow())
  {
    cout << "Your arrow ricochet killed the Wumpus, you lucky dog!" << endl;
    cout << "Accidental victory, but still you win!" << endl;
    cave.setEndGame(true);
  }
}//end of checkWampus()


//--------------------------------------------------------------------------------
//void function shooting has parameter of cave and int reference amountOfRooms
//this is a recursive function that will be called for the input of shooting.
//it will check the outcomes of shooting, and the possiblys 
void shooting(Cave &cave, int &amountOfRooms)
{
   int room;                //user input room
   int ricochet;            
   int notAdjCounter = 0;   //counter for non-adjacent rooms
   //loop that will run when the amountOfRooms is above 0
   while(amountOfRooms > 0)
   {
   cin >> room;             //gets user input
     
   //for loop that would check all adjacent rooms
   for(int i = 0; i < 3; i++)
   {
     //if an adjacent matched the user input
     if(room == adjacentRooms[cave.getArrow()][i])
     {
       //if the wampus was in that new room
       if(room == cave.getWumpus())
       {
         cout << "Wumpus has just been pierced by your deadly arrow!" << endl;
         cout << "Congratulations on your victory, you awesome hunter you." << endl;
         cave.setEndGame(true);//set end game to true
         amountOfRooms = 0;//set amount of rooms to 0
       }
       else
       {
         cave.setArrow(room);                 //set the arrow to the new room
         checkPerson(cave, amountOfRooms);    //check if the person is in the new room
         shooting(cave, --amountOfRooms);     //call the function recursivly
       }
     }
     else
     {
       notAdjCounter++;    //count for non-adjacent rooms
       //if the counter hits 3
       if(notAdjCounter == 3)
       {
         cout << "\nRoom " << room << " is not adjacent.  Arrow ricochets..." << endl;
         ricochet = lowestAdjacent(cave.getArrow());   //set ricochet to the lowest adjacent room of the current room
         room = ricochet;
         cave.setArrow(room);                          //set the arrow to the ricochet room
         checkPerson(cave, amountOfRooms, 1);          //check if the person is there
         checkWampus(cave);                            //check if wumpus is there
         shooting(cave, --amountOfRooms);              //call the function recursivly
       }
     }
   }//end of for loop
  }//end of while()
}


//--------------------------------------------------------------------------------
//void function checkPits() has a cave parameter
//This checks if the player had go in a room with a bottomless pit
void checkPits (Cave &cave)
{
  if(cave.getBottomlessPit1() == cave.getPerson() || cave.getBottomlessPit2() == cave.getPerson())
  {
    cout << "Aaaaaaaaahhhhhh...." << endl;
    cout << "    You fall into a pit and die." << endl;
    cave.setEndGame(true); //set end game to true
  }
}//end of checkPits()


//--------------------------------------------------------------------------------
//void function resetBats() has a cave parameter
//this reset the bat location when they have been used
int resetBats(Cave &cave)
{
  int location;
  //checks if the new location of the bats is not equal to the old location, bats 2 and the person
  do
  {
    location = randomNumber();
  }while(location == cave.getSuperBats1() || location == cave.getSuperBats2() || location == cave.getPerson());
  return location; //returns the new location
}//end of resetBats()


//--------------------------------------------------------------------------------
//void function checkBats() has a cave parameter
//this checks if the person went into a room with bats 
void checkBats(Cave &cave)
{  
  int playerLocation;
  if(cave.getSuperBats1() == cave.getPerson() || cave.getSuperBats2() == cave.getPerson())
  {
    //finds a new random location for the player that is not equal to their current
    do
    {
      playerLocation = randomNumber();
    }while(cave.getPerson() == playerLocation);
    cout << "\nWoah... you're flying!" <<endl;
    cout << "You've just been transported by bats to room " << playerLocation << endl;
    //checks which super bats were used, to see which one is to be reset
    if(cave.getSuperBats1() == cave.getPerson())
    {
      cave.setSuperBats1(resetBats(cave));
    }
    else if( cave.getSuperBats2() == cave.getPerson())
    {
      cave.setSuperBats2(resetBats(cave));
    }
      cave.setPerson(playerLocation);     //set the new location for the player
      checkWampus(cave);                  //checks if the wumpus is in the new room
      checkPits(cave);                    //checks if there is any pits in the new room
  }
}//end of checkBats()


//--------------------------------------------------------------------------------
//void function checkForArrow() with parameter cave
//this function cehcks when the player is put in a new room if there is an arrow
void checkForArrow(Cave &cave)
{
  if( cave.getPerson() == cave.getArrow())
  {
    cout << "Congratulations, you found the arrow and can once again shoot." << endl;
    cave.setArrow(-1); //set the array with the player
  }
}//end of checkForArrow()


//--------------------------------------------------------------------------------
//main() 
int main()
{   
    Cave cave;                       //object cave for class cave
    char moveType = ' ';             //user input of char
    int moveNumber = 1;              //counter for moveNumber
    bool valid;                      //Valid boolean
    int amountOfRooms;               //used for s input 
    int newRoom;                     //user input of new room
    srand(1);                        // Use this version in what you turn in
    // Seed the random number generator.  Change seed to time(0) to change output each time.
    // srand(time(0));

    setSuperBatsLocation(cave);          //sets the super bats
    setBottomlessPitLocation(cave);      //sets the bottomless pits
    setWumpusLocation(cave);             //sets the Wumpus
    setPlayerLocation(cave);             //sets the player
    do
    {
      playersCurrentRoom(cave);          //display current room
      adjacentHazards(cave);             //checks and displays adjacent hazards
      
      cout <<"\n"<< endl;
      displayMove(moveNumber);           //displays move
      
      cin >> moveType;                   //get user input and sets it to moveType
      moveType = toupper(moveType);      //makes all char inputs of moveType capitalized
      
      //if statement to determine what the user wants
      //Piture of the maze
      if(moveType == 'P')
      {
        displayCave();
      }
      //cheat, checking location of all objects
      if(moveType == 'C')
      {
        displayCurrentLocations(cave);
        continue;
      }
      //resets, gets user input and resets locations
      if(moveType == 'R')
      {
         resetGame(cave);
         moveNumber = 1;    //resets move counter
         continue;
      }
      //Move, moves the player
      if(moveType == 'M')
      {
        cin >> newRoom;                             //get user input of new rrom
        valid = checkIfValidRoom(cave,newRoom);     //checks if move is valid
          if(valid == true)
          {
            movingPlayer(cave,newRoom);             //moves the player
            checkForArrow(cave);                    //checks if the new room has an arrow
            checkWampus(cave);                      //checks if new room has a wumpus
            checkPits(cave);                        //checks if new room has a pits
            checkBats(cave);                        //checks if new room has a bats
          }
          else
          {
            continue;
          }
      }
      //Display instructions
      if(moveType == 'D')
      {
        displayInstructions();
      }
      //Shoot, user wants to shoot an arrow
      if(moveType == 'S')
      {
        //checks if the player has the arrow
        if(cave.getArrow() == -1)
        {
          shootingInstru();                    //shooting instructions
          cave.setArrow(cave.getPerson());     //set the arrow location to the person's location
          cin >> amountOfRooms;                //get the user input of how many rooms
          shooting(cave, amountOfRooms);       //calls the shooting function
          setWumpusAfter(cave);                //after shot, reset wumpus
        }
        else
        {
          cout << "\nSorry, you can't shoot an arrow you don't have.  Go find it." <<endl;
        }
      }
      //exit
      if(moveType == 'X')
      {
        cave.setEndGame(true);
      }
      
      cout << endl;                             
      moveNumber++;                        //Move counter
    }while(cave.getEndGame() == false);    //checks if the user wants to exit
    
    cout << "\nExiting Program ..." << endl;
}//end main()
