// prog3WhichLangMacbeth.cpp
//    Use letter frequency counts to determine what language text is in.
//
// Author: Jamil Naber
// Date: Oct 18, 2018
// Class: CS 141, Fall 2018
// Language: C++
//
// Translations of Macbeth were created online using Google translate, starting with the
// English version at http://shakespeare.mit.edu/macbeth/full.html
// To trigger the translation option, I went to the Chrome browser / Settings / Language
// option and changed the default language, which triggered showing the translation option
// for the English Macbeth page when it was displayed.  The translated text was then
// copy/pasted into a text file and saved.  The texts are stored using the multi-byte
// UTF-8 format, explained at https://en.wikipedia.org/wiki/UTF-8, though here we are
// not attempting to count more than the first 128 alphabetical characters.
//
/* Results of running program are:

Program 3: Which Language

Letter Frequency Counts:
	  Engl  Finn  Fren  Germ  Hung  Ital  Port  Span
A:   6018  9416  6544  5068  7541  8935  9939 10052
B:   1464   448  1081  2060  1746  1221  1173  1387
C:   2144   636  3028  3126  1014  3865  2855  3222
D:   3331  1013  2698  4592  2265  2945  3687  3815
E:   9270  7187 12782 14779  8280  9364 10551 10861
F:   1701   297  1101  1464   932  1099  1039   653
G:   1333   187   772  2503  2975  1423   983   936
H:   5244  2384  1004  4775  1687  1485  1413  1263
I:   4653  8022  5583  7062  2961  8128  4179  3964
J:     38  1331   538   148  1049     5   216   429
K:    691  3952    28  1080  3551    51    19    18
L:   3294  4648  4240  3089  4658  4814  2082  3985
M:   2481  3526  3269  2861  3812  2790  4415  3021
N:   4987  7958  5958  9026  4725  5966  4455  5704
O:   6054  4219  5315  2208  3529  8668  8898  7605
P:   1000  1331  2297   466   448  2060  1862  1842
Q:    121    78   907    84    77   548   954   883
R:   4518  1784  5986  5955  2802  5357  5328  5706
S:   4943  5425  6898  5852  4538  4863  6774  6468
T:   7055  7620  6025  5477  5632  5373  3729  3932
U:   2590  4226  5373  3327   963  3081  3840  3591
V:    657  1784  1566   661  1297  1287  1351   893
W:   1926   120    78  1797   138   141    68    61
X:    112    45   328    89    49    42   260    91
Y:   1637  1403   247   169  1998   216    65   891
Z:     15     3   343   939  2742   490   314   301

Letter frequency order:
	  Engl  Finn  Fren  Germ  Hung  Ital  Port  Span
		  E     A     E     E     E     E     E     E
		  T     I     S     N     A     A     A     A
		  O     N     A     I     T     O     O     O
		  A     T     T     R     N     I     S     S
		  H     E     R     S     L     N     R     R
		  N     S     N     T     S     T     N     N
		  S     L     I     A     M     R     M     L
		  I     U     U     H     K     S     I     I
		  R     O     O     D     O     L     U     T
		  D     K     L     U     G     C     T     D
		  L     M     M     C     I     U     D     U
		  U     H     C     L     R     D     C     C
		  M     R     D     M     Z     M     L     M
		  C     V     P     G     D     P     P     P
		  W     Y     V     O     Y     H     H     B
		  F     J     F     B     B     G     V     H
		  Y     P     B     W     H     V     B     G
		  B     D     H     F     V     B     F     V
		  G     C     Q     K     J     F     G     Y
		  P     B     G     Z     C     Q     Q     Q
		  K     F     J     V     U     Z     Z     F
		  V     G     Z     P     F     Y     X     J
		  Q     W     X     Y     P     W     J     Z
		  X     Q     Y     J     W     K     W     X
		  J     X     W     X     Q     X     Y     W
		  Z     Z     K     Q     X     J     K     K

Copy and paste a paragraph of text to be analyzed, followed by ^z (PC) or ^d (Mac): 
Ma per arrivare a un agreement bisogna essere in due. E dato che il governo intende resistere sui numeri della manovra, è necessario offrire garanzie all’Europa e ai mercati. Perciò sono stati stabiliti due capisaldi: uno tecnico, l’altro più politico. La riduzione strutturale del debito viene fissato come un «obiettivo strategico», non a caso sottolineato da Di Maio dopo il vertice. La linea dell’esecutivo è che per far ripartire l’Italia sia necessario «cambiare approccio» con una manovra espansiva «dopo anni di cure rigoriste senza risultati», ma s

A:51 B:5 C:20 D:15 E:55 F:4 G:6 H:2 I:54 J:0 K:0 L:22 M:10 N:29 O:39 P:13 Q:0 R:37 S:28 T:31 U:15 V:9 W:0 X:0 Y:0 Z:3 

Letter frequency order:
	  Engl  Finn  Fren  Germ  Hung  Ital  Port  Span  User
		  E     A     E     E     E     E     E     E     E
		  T     I     S     N     A     A     A     A     I
		  O     N     A     I     T     O     O     O     A
		  A     T     T     R     N     I     S     S     O
		  H     E     R     S     L     N     R     R     R
		  N     S     N     T     S     T     N     N     T
		  S     L     I     A     M     R     M     L     N
		  I     U     U     H     K     S     I     I     S
		  R     O     O     D     O     L     U     T     L
		  D     K     L     U     G     C     T     D     C
		  L     M     M     C     I     U     D     U     D
		  U     H     C     L     R     D     C     C     U
		  M     R     D     M     Z     M     L     M     P
		  C     V     P     G     D     P     P     P     M
		  W     Y     V     O     Y     H     H     B     V
		  F     J     F     B     B     G     V     H     G
		  Y     P     B     W     H     V     B     G     B
		  B     D     H     F     V     B     F     V     F
		  G     C     Q     K     J     F     G     Y     Z
		  P     B     G     Z     C     Q     Q     Q     H
		  K     F     J     V     U     Z     Z     F     J
		  V     G     Z     P     F     Y     X     J     K
		  Q     W     X     Y     P     W     J     Z     Q
		  X     Q     Y     J     W     K     W     X     W
		  J     X     W     X     Q     X     Y     W     X
		  Z     Z     K     Q     X     J     K     K     Y

		 60    78    16    50    94    10    18    13  <--- Difference (This line is not part of expected output, but is helpful!)

Language is Italian
 
 */
#include <iostream>  // For cin and cout
#include <cctype>    // For the letter checking functions     
#include <fstream>	 // For file input
#include <iomanip>   // For setw  
#include <cstdlib>   // For abs()
#include <stdio.h>   // File functions
#include <string>    // For string functions
#include <limits.h>  // For MAX_INT

using namespace std;

//Global Constants set
#define allLetters 26 // 26 letters
#define maxLangNum 9  // 8 languages and User

// initializing the functions
void outStagePick(int&);
void getLetterFreqCount(int, string);
void getLangAbb(string, string, string);
void outLetterFreqCount(int, string);
void setLetterFreqOrder(char);
void setLetterSort(char, int, int);
void outLetterFreqOrder(char, string, int);
void getUserParagraph(int);
void getWhichLanguage(char, int&);
void outWhichLanguage(int, string);

//Void function to have the output for the menu, and to set variable userSelection which is returned as a reference
void outStagePick(int &userSelection)
{
    cout <<"Program 3: Which Language.\n\n";
    cout <<"Select from the following stages of output to display:\n";
    cout << "\t1. Letter frequency counts\n";
    cout << "\t2. Letter frequency order\n";
    cout << "\t3. Get user input and display frequency counts\n";
    cout << "\t4. Get user input, display frequency counts, and display language\n";
    cout << "\t0. Exit the program\n";
    cout << "Your choice --> ";
    cin >> userSelection;
    cout << endl;
}//end of outStagePick function


//Void function getLetterFreqCount has variables letterFrequency and fileName, where letterFrequencyTbl is pass by reference
//The function opens all 8 files and stores the number of each kind of alphabetical Char in a 2D array
void getLetterFreqCount(int (&letterFrequencyTbl)[allLetters][maxLangNum], string fileName[])
{
  ifstream inputFileStream;
  char c = ' ';
  // For loop to go though all 8 languages
  for(int i = 0; i < maxLangNum -1; i++)
  {
  inputFileStream.open( fileName[i].c_str());
    //while loop to garb every char in the currently opened file
    while( inputFileStream >> c)
    {
       // checks if the char is alpha
      if( isalpha(c)) 
      {
          c = toupper(c);
          letterFrequencyTbl[c-'A'][i]++;  //This 2D array accumulates everytime if find a letter in the file
      }
    } 
    inputFileStream.close();
  }// end of for loop
} // end of function getLetterFreqCount


//Void function to get the abbreviation of the text file for the language output
//It uses the array langAbb that is called by reference, and the array fileName 
void getLangAbb(string (&langAbb)[maxLangNum],string (&langFull)[maxLangNum], string fileName[])
{
  //Loop runs for all the languages and only stores the 4 letter of the langauge name of the file in the langAbb array
  //This is done by the erase libaray
  for(int i = 0; i < maxLangNum-1; i++)
  {
    langAbb[i] = fileName[i].erase(0,7);
    langFull[i] = fileName[i].erase(fileName[i].size() -4, 4);
    langAbb[i] = fileName[i].erase(4,6);
  }//end of loop
  langAbb[maxLangNum -1] = "User"; // assign the last array element with "User"
}// end of getLangAbb Function


// Void function outLetterFreqCount is the output of the 2D array letterFrequency
// It utilizes 2D array letterFrequency and langAbb[]
void outLetterFreqCount(int letterFrequencyTbl[][maxLangNum],string langAbb[])
{
  cout <<"Letter Frequency Counts:\n"<< "   ";
  //For loop displays the language abbreviations, k < 8 so the "User" will not print out
  for(int k = 0 ; k < 8; k++)
  {
    cout << setw(6) << langAbb[k];
  }//end of loop
  cout << endl;
  //Two for loop to display the 2D array letterFrequency.
  //First Loop goes through the letters, Seacond loop goes through the languages
  for(int i = 0; i < allLetters; i++)
      {
        cout <<(char)(i+'A')<< ": "; // Letter Display
          for(int j = 0; j < maxLangNum - 1; j++)
          { 
              cout<< setw(6) <<letterFrequencyTbl[i][j];
          }//end Loop  two
       cout<<  endl;
      }//end loop one
}// end of outLetterFreqCount function


//Void function setLetterFreqOrder is to initializing the char values that will be stored in the 2D array letterFrequencyOrder.
//letterFrequencyOrder will be passes with a reference
void setLetterFreqOrder(char (&letterFrequencyOrder)[allLetters][maxLangNum])
{
   //Two for loops will be used
   //First one will be running the letters, and the seacond one will be running the langauges
   for(int i = 0; i < allLetters; i++)
   {
       for(int j = 0; j < maxLangNum; j++)
       {
           letterFrequencyOrder[i][j] = (char)(i+'A');//This sets the value of a char at [i][j]
       }//end of loop two
   }//end of loop one
}//end of setLetterFreqOrder function 


//Void function setLetterSort uses reference letterFrequencyOrder, and letterFrequencyTbl to sort the most frequent letter...
//...of a language to the top, and least to the bottom
//The kind of sort used is a bubble sort
//Also, there is a default variable named "withUser" to have this function be reused when the userInput is added 
void setLetterSort(char (&letterFrequencyOrder)[allLetters][maxLangNum],int letterFrequencyTbl[allLetters][maxLangNum], int withUser = 0)
{
  //temp values are created for the char and int vlaue
  char temp = 0;
  int temp2 = 0;
  int langWithUser = maxLangNum + withUser; //Checking if the function will run this will user input or not
  
  //Three loops are used, first one is for the picking which language, seacond one is for bubble sort pass,...
  //...and the third one is used inreference to current and pass to sort
  for (int i = 0; i < langWithUser -1; i++)
  {
    for(int pass = 1; pass < allLetters; pass++)
    {
      for(int current = 0;current < allLetters - pass; current++)
      {
       //The if statment checks if the curret position of the letterFreqency is less than the next position.
       //If true, a swap will occur
       if(letterFrequencyTbl[current][i] < letterFrequencyTbl[current + 1][i])
          {
            temp = letterFrequencyOrder[current][i];                               //stores the current letter in a temp
            temp2 = letterFrequencyTbl[current][i];                                //stroes the current integer in a temp
            letterFrequencyOrder[current][i] = letterFrequencyOrder[current+1][i]; //swaps
            letterFrequencyTbl[current][i] = letterFrequencyTbl[current+1][i];     //swaps
            letterFrequencyTbl[current+1][i] = temp2;                              //set the temp to a new position
            letterFrequencyOrder[current+1][i] = temp;                             //set the temp to a new position
          }//end of if
      }//end of loop three
    }//end of loop two
  }//end of loop one
}//end of setLetterSort function 


//Void Function outLetterFreqOrder uses 2D array letterFrequencyOrder, Array langAbb, and variable withUser
//This is a display function to display the 2D array letterFrequenceyOrder, and can reused with user input beacause..
//... of the default value of "withUser"
void outLetterFreqOrder(char letterFrequencyOrder[allLetters][maxLangNum],string langAbb[maxLangNum], int withUser = 0)
{
  cout <<"\n";
  cout <<"Letter Frequency order:"<< endl;
  int langWithUser = maxLangNum + withUser; //checks if function needs to run with user Input in mind
  //For Loop runs for all the languages displaying their abbreviations
  for(int k = 0 ; k < langWithUser - 1; k++)
  {
    cout << setw(6) <<langAbb[k];
  }
  cout << endl;
  //Two for loops that display the letter's in order using the 2D array letterFrequenceyOrder
  for(int i = 0; i < allLetters; i++)
      {
          for(int j = 0; j < langWithUser - 1; j++)
          { 
              cout<< setw(6) <<letterFrequencyOrder[i][j];
          }//end loop two
       cout<<  endl;
      }//end loop one
}//End of function outLetterFreqOrder


//Void Function getUserParagraph uses &letterFrequencyTbl to store the user data in columns 9
void getUserParagraph(int (&letterFrequencyTbl)[allLetters][maxLangNum])
{
  char c = ' ';
  cout << "\nCopy and paste a paragraph of text to be analyzed, followed by ^z (PC) or ^d (Mac): \n";
  //While loop that takes the input, and stores it as a char c
    while(cin >> c)
    {
      //Checks if char c is an alpha
      if( isalpha( c)) 
      {          
            c = toupper( c);        
            letterFrequencyTbl[ c-'A'][maxLangNum -1]++; //incerments the integer value in parrellel to the letter
        }
    }//end of while loop
  cout << "\n";
   //For loop that displays the users Input in Values of integer
   for (int i=0; i<allLetters; i++) 
   {
         cout << (char)(i+'A') << ":" << letterFrequencyTbl[i][maxLangNum -1] << " ";
   }//End of for loop
  cout << "\n";
}


//Void function getWhichLanguage has 2D array letterFrequencyTbl and a reference int in it scope
//This is used to determine what language the user input is  
void getWhichLanguage(char letterFrequencyOrder[allLetters][maxLangNum],int &languageValue)
{
    int minDiff = INT_MAX;
    int totalDiff;
    int diffInRank;
    //Three for loops are used, first one is the selection of the language the 2D array will be compared to user input...
    //..., the seacond one is to go through the letters of the users input, and the thrid is going through the letters ...
    //of the language at the selection of language
    for(int i = 0; i < maxLangNum -1; i++)
    {
      totalDiff = 0;
      diffInRank = 0;
        for (int j = 0; j < 26; j++)
        {
            for(int k = 0; k <26; k++)
            {
                if(letterFrequencyOrder[j][maxLangNum -1] == letterFrequencyOrder[k][i])
                {
                   diffInRank = abs(j - k); //Finds the differnce 
                   totalDiff = totalDiff + diffInRank; //totals up the differnce
                }
                
            }//end of thrid loop
        }//end of seacond loop
        if( totalDiff < minDiff)  //determines which language has the the least amount of differnces
        {
            minDiff = totalDiff;
            languageValue = i;
            
        }
    }//end of first loop
    //Freach and Portuguese fix
     if (languageValue == 2)// checks if the langauge is Freach 
     {
       //For loop checks the postion of a letter in the user input
       for(int l = 0; l < 6; l++)
       {
          //if the letter O is found in any posisition 
          if( letterFrequencyOrder[l][maxLangNum -1] == 'O')
          {
            languageValue = 6; //set the langauge to Portuguese
          }
       }// end of loop
     }
}


// Void function outWhichLanguage is the output of our results to which language our user Inputed
// Int languageValue and string langFull is pass in by the scope
void outWhichLanguage(int languageValue , string langFull[maxLangNum])
{
  cout << "\nLanguage is "<<langFull[languageValue] << endl;
}//End of Function outWhichLanguage


int main ()
{
  // Initializing variables for main
  int userSelection;                                       // User picks up to what stage to go to for the program 
  int languageValue = 0;                                   // finding the Langunge 
  string fileName[maxLangNum] = {"MacbethEnglish.txt", "MacbethFinnish.txt", "MacbethFrench.txt", "MacbethGerman.txt", "MacbethHungarian.txt", "MacbethItalian.txt", "MacbethPortuguese.txt", "MacbethSpanish.txt", "User"};
  string langAbb[maxLangNum];                              // The Langunge Abbreviation for output
  string langFull[maxLangNum];                             // Stores the full language name
  int letterFrequencyTbl[allLetters][maxLangNum]= {0};     // 2D array that holds the integer values of the Frequency of each letter in each language
  char letterFrequencyOrder[allLetters][maxLangNum];       // 2D array that holds the Char value of the sorted letters
  
  outStagePick(userSelection); //Finds which stage the use want to run till
  if(userSelection > 0)
  {
    getLetterFreqCount(letterFrequencyTbl, fileName); //Opens, reads and stores the file info
    getLangAbb(langAbb,langFull,fileName); // Gets the language abbreviation and language full string
    outLetterFreqCount(letterFrequencyTbl,langAbb);//Outputs the Frequency count of the letters
    if(userSelection > 1)
    {
      setLetterFreqOrder(letterFrequencyOrder); //Initializes the 2D array
      setLetterSort(letterFrequencyOrder, letterFrequencyTbl); // Bubble sorts both the 2D arrays
      outLetterFreqOrder(letterFrequencyOrder, langAbb); //Displays the sorted letterFrequencyOrder
      if(userSelection > 2)
      {
          getUserParagraph(letterFrequencyTbl); //Grabs the user input and stores its frequency in the 8th column
          setLetterSort(letterFrequencyOrder, letterFrequencyTbl, 1); // Sorts the letterFrequencyOrder and letterFrequencyTbl with the user data
          outLetterFreqOrder(letterFrequencyOrder, langAbb, 1); //Displays the letterFrequencyOrder with the user input
          if(userSelection > 3)
          {
            getWhichLanguage(letterFrequencyOrder, languageValue); // Finds out which language the user enters
            outWhichLanguage(languageValue, langFull); //Displays which langauge the user inputed
          }
      }
    }
  }
  
   return 0;
}//end of main


//***Park: Great Works