/* prog1Step4.cpp
 *    Play the dice game, where each user chooses the Red, Green or Blue die.
 * 
 * Class: CS 141
 * Author: Jamil Naber
 * Lab: Wed 9am
 * System:  C++ online in codio.com
 *
 *   45 Programming Style Rubric (Given only if program runs substantially correctly)
 *         5 This rubric is included in the program
 *        10 Variable names are meaningful and use camel-case (e.g. totalCount)
 *        10 Comments: Has header.  Comments on each significant block of code
 *        10 Appropriate data and control structures
 *        10 Code Layout: Appropriate indentation and blank lines
 */
#include <iostream>
#include <cstdlib>    // Needed for rand() to work
using namespace std;

int main()
{
    // Three arrays have been set for each color die
    // The values of the array have been inserted manually to the corrsponding side
    int redDie[6] = {1,4,4,4,4,4};
    int greenDie[6] = {2,2,2,5,5,5};
    int blueDie[6] = {6,3,3,3,3,3};
    string userInput;            // string to grab userInput
    string playerOne;            // the string that will be playerOne
    string playerTwo;            // the string that will be playerTwo
    int rawRandomNumber;         // storing the rand() function
    int diceFaceNumber;          // which side the die is on 
    int playerOneResult;         // the result for one roll for playerOne
    int playerTwoResult;         // the result for one roll for playerTwo
    int playerOneOverall = 0;    // the accumulator value to store the overall score for playerOne
    int playerTwoOverall = 0;    // the accumulator value to store the overall score for playerTwo
    
    // Beginning format  with simple cout libaray
    cout << "Class: CS 141" << endl;
    cout << "Lab: Tues 5am" << endl;
    cout << "System: C++ in codio.com" << endl;
    cout << "Welcome to the dice game, where you and your opponent each choose" << endl;
    cout << "one of the red, green or blue dice and roll them." << endl;
    cout << "\nThe dice contain the following sides:" << endl;
    
    // cout.width() is used to format the output inline with the others, that match with the colon
    cout.width(9);
    // Used the arrays to put out their variables via a loop with the varible i being the loop counter
    // Snice we know the highest index of each array is 5, we can set the loop limit to greater than or equal to 5
    // We used the i set in the loop to go though our index in each array    
    cout << right << "Red: " << left;                                                                                
    for(int i=0; i <=5; i++)                                                                                        
        {
            cout<< redDie[i] << " ";                                                                                   
        }
    cout<< endl;
    cout.width(9);
    cout << right <<"Green: "<< left;
    for(int i=0; i <=5; i++)
        {
            cout<< greenDie[i] << " "; 
        }
    cout << endl;
    cout.width(9);
    cout << right <<"Blue: "<< left;
    for(int i=0; i <=5; i++)
        {
            cout<< blueDie[i] << " "; 
        }
    cout << endl;
    
    cout << "\nEnter the die colors (R G or B): ";
    cin >> userInput; // Getting user input of the colors 
    cout << endl;
    
    // *** Arvan: Keep It Simple, Stupid (K.I.S.S.).
    // *** Arvan: You could have used userInput.at(0);
    playerOne = userInput.substr(0,1); // taking the first letter in and setting it as playerOne                                                                            
    playerTwo = userInput.substr(1);   // taking the second letter in and setting it as playerTwo                                                                              
    
    for (int j = 1; j <= 50; j++) // Loop that rolls the dice 50 times, each time having a winner
    {
        rawRandomNumber = rand(); //using rand for the first instance
        diceFaceNumber = rawRandomNumber % 6;// % gives me the possible number of 0 to 5
        if (playerOne == "R")// This if statment is to check what playerOns's value is equal to
        {
            // Once it passes the if condition, we can set the rand() function inside the index of the red array
            // , which would align it with its corresponding number on that side
            playerOneResult = redDie[diceFaceNumber];
        }
        // *** Arvan: Write more meaningful comments. 
        else if (playerOne == "G")
        {
            playerOneResult = greenDie[diceFaceNumber];
        }
        else if(playerOne == "B")
        {
            playerOneResult = blueDie[diceFaceNumber];
        }
        // *** Arvan: Write more meaningful comments. 
        rawRandomNumber = rand(); // needs to be reused 
        diceFaceNumber = rawRandomNumber % 6;
        if (playerTwo == "R")
        {
            playerTwoResult = redDie[diceFaceNumber];
        }
        else if (playerTwo == "G")
        {
            playerTwoResult = greenDie[diceFaceNumber];
        }
        else if(playerTwo == "B")
        {
            playerTwoResult = blueDie[diceFaceNumber];
        }
        
        if (playerOneResult > playerTwoResult) // Compares the result of both players to see who won
        {
            playerOneOverall++; //if the conditon is true above, playerOne gets one added to their score
        }
        else
        {
            playerTwoOverall++; 
        }
        cout << "  " << j << ". "<< playerOne << ":" << playerOneResult << " " << playerTwo << ":" << playerTwoResult<< ", Score: "<< playerOneOverall <<" to " << playerTwoOverall<<endl;
    }
    return 0;
}//end main()